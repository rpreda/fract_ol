#include <mlx.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <time.h>//use clock to determine if the processor is busy or not
# define UP 126
# define DOWN 125
# define LEFT 123
# define RIGHT 124
# define WIN_SIZE 600
# define SCROLL_UP 4
# define SCROLL_DOWN 5
# define ONE 18
# define TWO 19
# define THREE 20
# define FOUR 21


//16183215 nice contrast
//1239567 color vommit
//3215 monocolor green
int color_table[500];//works untill 500 iterations where I should never end up going
int max_iter = 80;//initial max iteration
int color_mult = 1239567;//16183215;//1239567;
int lock_params = 1;
long double re_c = -0.8;//-0.835;//0.39;//0.368;
long double im_c = 0.156;//-0.2321;//-0.39;//-0.368;
long double ox = WIN_SIZE / 2, oy = WIN_SIZE / 2;
long double zoom = 180;
long double zoom_alter = 1.6;
long double mv_x;
long double mv_y;
long double mv_step = 0.1;
void *image;
void *img;
int ln_size;
int scrap;
void *mlx_ptr;
void *win_ptr;
void *(*fractal_thread)(void*);
void pre_compute_color();

char blowing_up(long double re, long double im)
{
	if ((long double)(re * re + im * im) > 4)
		return (1);
	return (0);
}

static void	put_pixel(char *image, int x, int y, int col)
{
		int offset;

		offset = x * 4;
		offset += y * ln_size;
		*(int *)(image + offset) = col;
}
void *julia_thread(void *param)
{
	int x, y, temp;
	long double re, aux, im;
	int start = ((int *)param)[0];
	int end = ((int *)param)[1];
	int iterations;


	for (x = start; x < end; x++)
		for (y = 0; y < WIN_SIZE; y++)
		{
			re = (x - ox) / zoom;
			im = (y - oy) / zoom;
			iterations = 0;
			while (!blowing_up(re, im) && iterations < max_iter)
			{
				aux = re;
				re = re * re - im * im + re_c;
				im = 2 * aux * im + im_c;
				iterations++;
			}
			put_pixel(img, x, y, color_table[iterations]);
		}
	return NULL;
}

void *ship_thread(void *param)
{
	long double rex, imy, re_z, im_z, aux;
	int x, y, iterations;
	int start = ((int *)param)[0];
	int end = ((int *)param)[1];

	for (x = start; x < end; x++)
		for (y = 0; y < WIN_SIZE; y++)
		{
			rex = (x - ox) / zoom;
			imy = (y - oy) / zoom;
			iterations = 0;
			im_z = re_z = 0;
			while (!blowing_up(re_z, im_z) && iterations < max_iter)
			{
				aux = re_z;
				re_z = re_z * re_z - im_z * im_z + rex;
				im_z = 2 * aux * im_z + imy;
				if (re_z < 0)
					re_z = -re_z;
				if (im_z < 0)
					im_z = -im_z;
				iterations++;
			}
			put_pixel(img, x, y, color_table[iterations]);
		}
	return NULL;
}

void *mandel_thread(void *param)
{
	long double rex, imy, re_z, im_z, aux;
	int x, y, iterations;
	int start = ((int *)param)[0];
	int end = ((int *)param)[1];

	for (x = start; x < end; x++)
		for (y = 0; y < WIN_SIZE; y++)
		{
			rex = (x - ox) / zoom;
			imy = (y - oy) / zoom;
			iterations = 0;
			im_z = re_z = 0;
			while (!blowing_up(re_z, im_z) && iterations < max_iter)
			{
				aux = re_z;
				re_z = re_z * re_z - im_z * im_z + rex;
				im_z = 2 * aux * im_z + imy;
				iterations++;
			}
			put_pixel(img, x, y, color_table[iterations]);
		}
	return NULL;
}

void fractal_draw()
{
	int x;
	int y;
	int temp;
	int st[] = {0, 150};
	int st1[] = {150, 300};
	int st2[] = {300, 450};
	int st3[] = {450, 600};

	long double re;
	long double aux;
	long double im;
	pthread_t help_thread;
	pthread_t help_thread2;
	pthread_t help_thread3;
	pthread_t help_thread4;

	if (image != NULL)
		mlx_destroy_image(mlx_ptr, image);
	image = mlx_new_image(mlx_ptr, WIN_SIZE, WIN_SIZE);
	img = mlx_get_data_addr(image, &scrap, &ln_size, &scrap);
	pthread_create(&help_thread, NULL, fractal_thread, st);
	pthread_create(&help_thread2, NULL, fractal_thread, st1);
	pthread_create(&help_thread3, NULL, fractal_thread, st2);
	pthread_create(&help_thread4, NULL, fractal_thread, st3);
	pthread_join(help_thread, NULL);
	pthread_join(help_thread2, NULL);
	pthread_join(help_thread3, NULL);
	pthread_join(help_thread4, NULL);
	mlx_clear_window(mlx_ptr, win_ptr);
	mlx_put_image_to_window(mlx_ptr, win_ptr, image, 0, 0);
}

int cursor_hook(int x, int y)
{
	if (!lock_params)
	{
		re_c = x / 155;
		im_c = -y / 185;
		fractal_draw();
	}
	return (0);
}

int scroll_hook(int buttoncode, int x, int y, void *data)
{
	//printf("%d\n", buttoncode);
	if (buttoncode == SCROLL_UP)//q == zoom out
	{
		if (max_iter >= 98)
		{
			if (max_iter < 150)
				max_iter -= 2;
			else
				max_iter -= 4;
		}
		ox = x - (x - ox) / zoom_alter;
		oy = y - (y - oy) / zoom_alter;
		if (zoom >= 180)
			zoom /= zoom_alter;
		fractal_draw();
	}
	if (buttoncode == SCROLL_DOWN)
	{
		if (max_iter < 150)
			max_iter += 2;
		else if (max_iter < 500)
			max_iter += 4;
		ox = x - (x - ox) * zoom_alter;
		oy = y - (y - oy) * zoom_alter;
		zoom *= zoom_alter;
		fractal_draw();
	}
	return (0);
}
int key_hook(int keycode, void *scrap)
{
	(void)scrap;
	//printf("%d\n", keycode);
	if (keycode == 8)//c
		lock_params = !lock_params;
	if (keycode == ONE)
	{
		color_mult = 1239567;
		pre_compute_color();
		fractal_draw();
	}
	if (keycode == TWO)
	{
		color_mult = 3215;
		pre_compute_color();
		fractal_draw();
	}
	if (keycode == 0 && max_iter < 1000)//a
	{
		max_iter += 10;
		fractal_draw();
	}
	if (keycode == 2 && max_iter > 20)//b
	{
		max_iter -= 10;
		fractal_draw();
	}
	if (keycode == 53)
		exit(0);
	if (keycode == 15)//r
	{
		zoom = 180;
		ox = WIN_SIZE / 2;
		oy = WIN_SIZE / 2;
		max_iter = 100;
		fractal_draw();
	}
	if (keycode == UP || keycode == DOWN || keycode == LEFT || keycode == RIGHT)
	{
		mv_step = 30;
		if (keycode == UP)
			oy -= mv_step;
		if (keycode == DOWN)
			oy += mv_step;
		if (keycode == LEFT)
			ox -= mv_step;
		if (keycode == RIGHT)
			ox += mv_step;
		fractal_draw();
	}
	if (keycode == 12)
	{
		if (max_iter < 150)
			max_iter -= 2;
		else
			max_iter -= 4;
		zoom /= zoom_alter;
		fractal_draw();
	}
	if (keycode == 14)
	{
		if (max_iter < 150)
			max_iter += 2;
		else if (max_iter < 500)
			max_iter += 4;
		zoom *= zoom_alter;
		fractal_draw();
	}
	return (0);
}

void pre_compute_color()
{
	int i;

	i = 0;
	while (i < 500)
	{
		color_table[i] = color_mult * i;
		i++;
	}
}

int main(int argc, char **argv)
{
	if (argc > 1 && argv[1][0] == 'M')
	{
		color_mult = 1239567;
		fractal_thread = mandel_thread;
	}
	else if (argc > 1 && argv[1][0] == 'J')
		fractal_thread = julia_thread;
	else if (argc > 1 && argv[1][0] == 'S')
		fractal_thread = ship_thread;
	else
	{
		printf("%s\n", "Available parameters are M-mandelbrot, J-julia and S-Burning Ship");
		exit (0);
	}
	mlx_ptr = mlx_init();
	win_ptr = mlx_new_window(mlx_ptr, WIN_SIZE, WIN_SIZE, "Fract'ol");
	mlx_key_hook(win_ptr, key_hook, NULL);
	mlx_hook(win_ptr, 4, (1L << 2), scroll_hook, NULL);
	mlx_hook(win_ptr, 6, (1L << 6), cursor_hook, NULL);
	pre_compute_color();
	fractal_draw();
	mlx_loop(mlx_ptr);
}
